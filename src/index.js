import express from 'express';
import cors from 'cors';

import canonize from './canonize';

// https://git-scm.com/book/ru/v1/Основы-Git-Запись-изменений-в-репозиторий

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.json({
    hello: 'Hellio World! ЗАГОТОВКА',
  });
});

// http://localhost:3000/task2c/?username=https://vk.com/skillbranch
// http://localhost:3000/task2c/?username=//vk.com/skillbranch
// http://localhost:3000/task2c/?username=skillbranch
// http://localhost:3000/task2c/?username=https://vk.com/skillbranch?w=wall-117903599_1076
// http://localhost:3000/task2c/?username=http://www.vk.com/durov
// http://localhost:3000/task2c/?username=https://github.com/kriasoft/react-starter-kit
// http://localhost:3000/task2c/?username=https://github.com/kriasoft/react-starter-kit
// http://localhost:3000/task2c/?username=http://xn--80adtgbbrh1bc.xn--p1ai/pavel.durov
// http://localhost:3000/task2c/?username=https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750

app.get('/task2c', (req, res) => {
  let rez = req.query.username;
  console.log('до канонайз');
  rez = canonize(rez)[6];
  console.log('после канонайз');
  console.log('rez='+rez);
  // const re2 = new RegExp('@?([a-zA-Z0-9]*(.?[a-zA-Z0-9]*)?)', 'i');
  //rez = 'ashj/khnm.bdf/qwerty'
  const re2 = new RegExp('@?([a-zA-Z0-9._]*)', 'i');
  rez = rez.match(re2)[1];
  console.log('rez=');
  console.log(rez);
  if (rez[0] !== '@') {
    rez = '@' + rez;
  }
  res.send(rez.toString());
});

// komment
// http://localhost:3000/task2b/?fullname=Steven Paul Jobs
function test_response(ss) {
    let testrez = false;
    console.log(ss.toString());
    // let i=0;
    let elem_ss = 'asdf';
    // for (i=0 ; i<=ss.length ; i++)
    console.log('qwerty ss=');
    console.log(ss);
    for(var i = 0; i < ss.length; i++){
    // for (elem_ss in ss)
      console.log('i=');
      console.log(i);
      elem_ss = ss[i];
      console.log('elem_ss=');
      console.log(elem_ss);

      let poz = elem_ss.search(/[^a-zA-Zа-яА-ЯёЁó']/);
      console.log('poz='+poz);
      if (poz >= 0) {
        return false;
      } else {
        testrez = true;
      };
    };
    return testrez;
}

function fist_up(s) {
  return s.toUpperCase().slice(0,1) + s.toLowerCase().slice(1);
}

app.get('/task2b', (req, res) => {
  let rez = req.query.fullname;
  console.log(rez.toString());
  let ts = rez;
  ts = ts.toString();
  ts = ts.trim();
  // Split String
  let ss = ts.split(/\s+/);
  let fam = 'фамилия';
  let imya = 'имя';
  let och = 'отчество';
  if (ss.length === 1) {
    if (ss[0] === '') {
      rez = 'Invalid fullname';
    } else {
      ss[0] = fist_up(ss[0]);
      fam = ss[0];
      rez = fam;
    }
  }
  if (ss.length === 2) {
    ss[0] = fist_up(ss[0]);
    ss[1] = fist_up(ss[1]);
    fam = ss[1];
    imya = ss[0];
    rez = '' + fam.toString() + ' ' + imya[0].toString() + '.';
  }
  if (ss.length === 3) {
    ss[0] = fist_up(ss[0]);
    ss[1] = fist_up(ss[1]);
    ss[2] = fist_up(ss[2]);
    fam = ss[2];
    imya = ss[0];
    och = ss[1]
    rez = '' + fam.toString() + ' ' + imya[0].toString() + '. ' + och[0].toString() + '.';
  }
  console.log('вызываем test_response ss=');
  console.log(ss);
  if (ss.length > 3 || ss.length === 0 || !test_response(ss)) {
    rez = 'Invalid fullname';
  }
  res.send(rez.toString());
});
// http://localhost:3000/task2A/?a=2&b=82
app.listen(3000, function () {
  console.log('my task2c Example app listening on port 3000!');
});
